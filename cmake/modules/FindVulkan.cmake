if( NOT Vulkan_BASE )
	message( "Vulkan_BASE has not been set. Set this variable to point to your LunarG Vulkan SDK installation" )
endif()

find_path( Vulkan_INCLUDE_DIR NAMES vulkan/vulkan.h PATHS ${Vulkan_BASE} PATH_SUFFIXES macOS/include Include x86_64/include )
find_library( Vulkan_LIBRARY NAMES vulkan vulkan-1 PATHS ${Vulkan_BASE} PATH_SUFFIXES macOS/lib Lib Bin x86_64/lib )
find_program( VulkanShaderCompile_PROGRAM glslc PATHS ${Vulkan_BASE} PATH_SUFFIXES macOS/bin bin x86_64/bin )
if( APPLE )
	find_program( MSLShaderCompile_PROGRAM spirv-cross PATHS ${Vulkan_BASE} PATH_SUFFIXES macOS/bin )
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args( Vulkan REQUIRED_VARS Vulkan_INCLUDE_DIR Vulkan_LIBRARY VulkanShaderCompile_PROGRAM )

if( Vulkan_FOUND AND NOT TARGET Vulkan::Vulkan )
	add_library( Vulkan::Vulkan INTERFACE IMPORTED )
	set_target_properties( Vulkan::Vulkan PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${Vulkan_INCLUDE_DIR}"
			INTERFACE_LINK_LIBRARIES "${Vulkan_LIBRARY}" )
endif()
