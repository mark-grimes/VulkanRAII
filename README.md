# VulkanRAII

Classes to help with Vulkan object management using Resource Acquisition Is Initialization (RAII) principles.

You probably don't want to use this library, use the `vk::UniqueHandle<...>` class from [Vulkan-Hpp](https://github.com/KhronosGroup/Vulkan-Hpp)
which apparently is included in the Vulkan SDK.

Basically, I read as far as

> Keep in mind that Vulkan-Hpp does not support RAII style handles and that you have to cleanup your resources in the error handler!

in the Vulkan-Hpp description and thought that was stupid so wrote this library, as every man and his dog seems
to have done for Vulkan. I've not used `vk::UniqueHandle<...>` but it looks like it should do what the main
point of this library was supposed to be.
