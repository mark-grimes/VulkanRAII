#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class DescriptorPool;
	class DescriptorSetLayout;
}

namespace vkraii
{
	class DescriptorSet
	{
	public:
		explicit DescriptorSet( std::nullptr_t null );
		DescriptorSet( const vkraii::Device& device, const vkraii::DescriptorPool& descriptorPool, const vkraii::DescriptorSetLayout& descriptorSetLayout );
		DescriptorSet( const DescriptorSet& other ) = delete;
		DescriptorSet( DescriptorSet&& other );
		DescriptorSet& operator=( const DescriptorSet& other ) = delete;
		DescriptorSet& operator=( DescriptorSet&& other );
		~DescriptorSet();
		inline VkDescriptorSet native_handle() { return descriptorSet_; }
		inline const VkDescriptorSet native_handle() const { return descriptorSet_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkDescriptorPool descriptorPool_=VK_NULL_HANDLE;
		VkDescriptorSet descriptorSet_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
