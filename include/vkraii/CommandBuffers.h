#pragma once

#include <vulkan/vulkan.h>
#include <vector>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class CommandBuffers
	{
	public:
		explicit CommandBuffers( std::nullptr_t null );
		CommandBuffers( const vkraii::Device& device, const VkCommandBufferAllocateInfo& allocateInfo );
		CommandBuffers( const CommandBuffers& other ) = delete;
		CommandBuffers( CommandBuffers&& other );
		CommandBuffers& operator=( const CommandBuffers& other ) = delete;
		CommandBuffers& operator=( CommandBuffers&& other );
		~CommandBuffers();
		/// Returns the first (usually only) VkCommandBuffer
		inline VkCommandBuffer native_handle() { return (commandBuffers_.empty() ? nullptr : commandBuffers_.front()); }
		/// Returns the first (usually only) VkCommandBuffer
		inline const VkCommandBuffer native_handle() const { return (commandBuffers_.empty() ? nullptr : commandBuffers_.front()); }
		/// The number of VkCommandBuffers
		inline uint32_t size() const { return static_cast<uint32_t>( commandBuffers_.size() ); }
		/// Returns the start of an array of all VkCommandBuffer. The size is given by size().
		inline VkCommandBuffer* native_handle_array() { return commandBuffers_.data(); }
		/// Returns the start of an array of all VkCommandBuffer. The size is given by size().
		inline const VkCommandBuffer* native_handle_array() const { return commandBuffers_.data(); }
		/// Returns the nth VkCommandBuffer
		inline VkCommandBuffer native_handle( unsigned index ) { return commandBuffers_[index]; }
		/// Returns the nth VkCommandBuffer
		inline const VkCommandBuffer native_handle( unsigned index ) const { return commandBuffers_[index]; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkCommandPool commandPool_=VK_NULL_HANDLE;
		std::vector<VkCommandBuffer> commandBuffers_;
	};
} // end of namespace vkraii
