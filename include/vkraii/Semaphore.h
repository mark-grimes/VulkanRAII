#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class Semaphore
	{
	public:
		explicit Semaphore( std::nullptr_t null );
		Semaphore( const vkraii::Device& device );
		Semaphore( const Semaphore& other ) = delete;
		Semaphore( Semaphore&& other );
		Semaphore& operator=( const Semaphore& other ) = delete;
		Semaphore& operator=( Semaphore&& other );
		~Semaphore();
		inline VkSemaphore native_handle() { return semaphore_; }
		inline const VkSemaphore native_handle() const { return semaphore_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkSemaphore semaphore_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
