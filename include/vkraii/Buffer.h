#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class DeviceMemory;
}

namespace vkraii
{
	class Buffer
	{
	public:
		explicit Buffer( std::nullptr_t null );
		Buffer( VkDevice device, const VkBufferCreateInfo& createInfo );
		Buffer( const vkraii::Device& device, const VkBufferCreateInfo& createInfo );
		Buffer( const Buffer& other ) = delete;
		Buffer( Buffer&& other );
		Buffer& operator=( const Buffer& other ) = delete;
		Buffer& operator=( Buffer&& other );
		~Buffer();
		VkMemoryRequirements memoryRequirements() const;
		void bindBufferMemory( const vkraii::DeviceMemory& memory, VkDeviceSize offset=0 );
		inline VkBuffer native_handle() { return buffer_; }
		inline const VkBuffer native_handle() const { return buffer_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkBuffer buffer_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
