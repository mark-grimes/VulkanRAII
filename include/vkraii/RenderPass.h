#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class RenderPass
	{
	public:
		explicit RenderPass( std::nullptr_t null );
		RenderPass( const vkraii::Device& device, const VkRenderPassCreateInfo& createInfo );
		RenderPass( const RenderPass& other ) = delete;
		RenderPass( RenderPass&& other );
		RenderPass& operator=( const RenderPass& other ) = delete;
		RenderPass& operator=( RenderPass&& other );
		~RenderPass();
		inline VkRenderPass native_handle() { return renderPass_; }
		inline const VkRenderPass native_handle() const { return renderPass_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkRenderPass renderPass_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
