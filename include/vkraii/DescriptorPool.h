#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class DescriptorPool
	{
	public:
		explicit DescriptorPool( std::nullptr_t null );
		DescriptorPool( const vkraii::Device& device, const VkDescriptorPoolCreateInfo& createInfo );
		DescriptorPool( const DescriptorPool& other ) = delete;
		DescriptorPool( DescriptorPool&& other );
		DescriptorPool& operator=( const DescriptorPool& other ) = delete;
		DescriptorPool& operator=( DescriptorPool&& other );
		~DescriptorPool();
		bool freeDescriptorSetBit() const;
		inline VkDescriptorPool native_handle() { return descriptorPool_; }
		inline const VkDescriptorPool native_handle() const { return descriptorPool_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkDescriptorPool descriptorPool_=VK_NULL_HANDLE;
		bool freeDescriptorSetBit_=false;
	};
} // end of namespace vkraii
