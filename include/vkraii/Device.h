#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>

namespace vkraii
{
	class Device
	{
	public:
		explicit Device( std::nullptr_t null );
		Device( VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo& createInfo );
		Device( const Device& other ) = delete;
		Device( Device&& other );
		Device& operator=( const Device& other ) = delete;
		Device& operator=( Device&& other );
		~Device();
		VkQueue queueHandle( unsigned queueNumber );
		inline VkDevice native_handle() { return device_; }
		inline const VkDevice native_handle() const { return device_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		uint32_t queueFamilyIndex_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
