#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class Fence
	{
	public:
		explicit Fence( std::nullptr_t null );
		Fence( const vkraii::Device& device, bool createSignaled=true );
		Fence( const Fence& other ) = delete;
		Fence( Fence&& other );
		Fence& operator=( const Fence& other ) = delete;
		Fence& operator=( Fence&& other );
		~Fence();
		bool status() const;
		void wait() const;
		void reset();
		inline VkFence native_handle() { return fence_; }
		inline const VkFence native_handle() const { return fence_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkFence fence_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
