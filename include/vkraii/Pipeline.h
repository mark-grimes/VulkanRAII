#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class PipelineCache;
}

namespace vkraii
{
	class Pipeline
	{
	public:
		explicit Pipeline( std::nullptr_t null );
		Pipeline( const vkraii::Device& device, const VkGraphicsPipelineCreateInfo& createInfo );
		Pipeline( const vkraii::Device& device, const VkComputePipelineCreateInfo& createInfo );

		Pipeline( const vkraii::Device& device, vkraii::PipelineCache& pipelineCache, const VkGraphicsPipelineCreateInfo& createInfo );
		Pipeline( const vkraii::Device& device, vkraii::PipelineCache& pipelineCache, const VkComputePipelineCreateInfo& createInfo );

		Pipeline( const Pipeline& other ) = delete;
		Pipeline( Pipeline&& other );
		Pipeline& operator=( const Pipeline& other ) = delete;
		Pipeline& operator=( Pipeline&& other );
		~Pipeline();
		inline VkPipeline native_handle() { return pipeline_; }
		inline const VkPipeline native_handle() const { return pipeline_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkPipeline pipeline_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
