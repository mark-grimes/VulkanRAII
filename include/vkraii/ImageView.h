#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class ImageView
	{
	public:
		explicit ImageView( std::nullptr_t null );
		ImageView( const vkraii::Device& device, const VkImageViewCreateInfo& createInfo );
		ImageView( VkDevice device, const VkImageViewCreateInfo& createInfo );
		ImageView( const ImageView& other ) = delete;
		ImageView( ImageView&& other );
		ImageView& operator=( const ImageView& other ) = delete;
		ImageView& operator=( ImageView&& other );
		~ImageView();
		inline VkImageView native_handle() { return imageView_; }
		inline const VkImageView native_handle() const { return imageView_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkImageView imageView_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
