#pragma once

#include <vulkan/vulkan.h>
#include <string>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class ShaderModule
	{
	public:
		explicit ShaderModule( std::nullptr_t null );
		/** @brief Create from a SPIR-V file given by the filename */
		ShaderModule( const vkraii::Device& device, const std::string& filename, VkShaderStageFlagBits shaderStage );
		/** @brief Create from a SPIR-V data in memory */
		ShaderModule( const vkraii::Device& device, void const* pData, size_t size, VkShaderStageFlagBits shaderStage );
		ShaderModule( const ShaderModule& other ) = delete;
		ShaderModule( ShaderModule&& other );
		ShaderModule& operator=( const ShaderModule& other ) = delete;
		ShaderModule& operator=( ShaderModule&& other );
		~ShaderModule();
		VkPipelineShaderStageCreateInfo& pipelineShaderStageCreateInfo();
		const VkPipelineShaderStageCreateInfo& pipelineShaderStageCreateInfo() const;
		inline VkShaderModule native_handle() { return module_; }
		inline const VkShaderModule native_handle() const { return module_; }

		/** @brief Convenience constructor to create from data in a container */
		template<typename T_container>
		ShaderModule( const T_container& container, VkShaderStageFlagBits shaderStage, const vkraii::Device& device ) : ShaderModule( container.data(), container.size(), shaderStage, device ) {}
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkShaderModule module_=VK_NULL_HANDLE;
		VkPipelineShaderStageCreateInfo shaderStageCreateInfo_={};
		void initFromMemory( void const* pData, size_t size, VkShaderStageFlagBits shaderStage );
	};
} // end of namespace vkraii
