#pragma once

#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class DeviceMemory;
}

namespace vkraii
{
	/** @brief Map a memory object into application address space, and unmap it on destruction.
	 *
	 * Note that this class does not correspond to a Vulkan object.
	 *
	 * @author Mark Grimes (mark.grimes@datanomita.com)
	 * @date 27/Sep/2018
	 * @copyright Copyright Mark Grimes 2018. License to be decided.
	 */
	class MappedMemory
	{
	public:
		MappedMemory( const vkraii::Device& device, vkraii::DeviceMemory& memory, VkDeviceSize size=VK_WHOLE_SIZE, size_t offset=0 );
		MappedMemory( const MappedMemory& other ) = delete;
		MappedMemory( MappedMemory&& other );
		MappedMemory& operator=( const MappedMemory& other ) = delete;
		MappedMemory& operator=( MappedMemory&& other );
		~MappedMemory();
		template<typename T> T* pointer() { return reinterpret_cast<T*>(pData_); }
		template<typename T> const T* pointer() const { return reinterpret_cast<T*>(pData_); }

		/** @brief Unmap the memory early (i.e. before destruction)
		 *
		 * This is not required for normal use. */
		void unmap();
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkDeviceMemory memory_=VK_NULL_HANDLE;
		void* pData_=nullptr;
	};
} // end of namespace vkraii
