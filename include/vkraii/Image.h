#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class DeviceMemory;
}

namespace vkraii
{
	class Image
	{
	public:
		explicit Image( std::nullptr_t null );
		Image( const vkraii::Device& device, const VkImageCreateInfo& createInfo );
		Image( VkDevice device, const VkImageCreateInfo& createInfo );
		Image( const Image& other ) = delete;
		Image( Image&& other );
		Image& operator=( const Image& other ) = delete;
		Image& operator=( Image&& other );
		~Image();
		VkFormat format() const;
		VkImageType imageType() const;
		VkMemoryRequirements memoryRequirements() const;
		void bindImageMemory( const vkraii::DeviceMemory& memory, VkDeviceSize offset=0 );
		inline VkImage native_handle() { return image_; }
		inline const VkImage native_handle() const { return image_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkImage image_=VK_NULL_HANDLE;
		VkFormat format_=VK_FORMAT_UNDEFINED;
		VkImageType imageType_=VK_IMAGE_TYPE_2D;
	};
} // end of namespace vkraii
