#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class Framebuffer
	{
	public:
		explicit Framebuffer( std::nullptr_t null );
		Framebuffer( const vkraii::Device& device, const VkFramebufferCreateInfo& createInfo );
		Framebuffer( const Framebuffer& other ) = delete;
		Framebuffer( Framebuffer&& other );
		Framebuffer& operator=( const Framebuffer& other ) = delete;
		Framebuffer& operator=( Framebuffer&& other );
		~Framebuffer();
		inline VkFramebuffer native_handle() { return framebuffer_; }
		inline const VkFramebuffer native_handle() const { return framebuffer_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkFramebuffer framebuffer_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
