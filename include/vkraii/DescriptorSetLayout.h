#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class DescriptorSetLayout
	{
	public:
		explicit DescriptorSetLayout( std::nullptr_t null );
		DescriptorSetLayout( const vkraii::Device& device, const VkDescriptorSetLayoutCreateInfo& createInfo );
		DescriptorSetLayout( const DescriptorSetLayout& other ) = delete;
		DescriptorSetLayout( DescriptorSetLayout&& other );
		DescriptorSetLayout& operator=( const DescriptorSetLayout& other ) = delete;
		DescriptorSetLayout& operator=( DescriptorSetLayout&& other );
		~DescriptorSetLayout();
		inline VkDescriptorSetLayout native_handle() { return descriptorSetLayout_; }
		inline const VkDescriptorSetLayout native_handle() const { return descriptorSetLayout_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkDescriptorSetLayout descriptorSetLayout_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
