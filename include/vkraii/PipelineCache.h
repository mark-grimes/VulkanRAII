#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class PipelineCache
	{
	public:
		explicit PipelineCache( std::nullptr_t null );
		PipelineCache( const vkraii::Device& device, const VkPipelineCacheCreateInfo& createInfo );
		PipelineCache( const PipelineCache& other ) = delete;
		PipelineCache( PipelineCache&& other );
		PipelineCache& operator=( const PipelineCache& other ) = delete;
		PipelineCache& operator=( PipelineCache&& other );
		~PipelineCache();
		inline VkPipelineCache native_handle() { return pipelineCache_; }
		inline const VkPipelineCache native_handle() const { return pipelineCache_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkPipelineCache pipelineCache_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
