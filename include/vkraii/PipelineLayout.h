#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class PipelineLayout
	{
	public:
		explicit PipelineLayout( std::nullptr_t null );
		PipelineLayout( const vkraii::Device& device );
		PipelineLayout( const vkraii::Device& device, const VkPipelineLayoutCreateInfo& createInfo );
		PipelineLayout( const PipelineLayout& other ) = delete;
		PipelineLayout( PipelineLayout&& other );
		PipelineLayout& operator=( const PipelineLayout& other ) = delete;
		PipelineLayout& operator=( PipelineLayout&& other );
		~PipelineLayout();
		inline VkPipelineLayout native_handle() { return pipelineLayout_; }
		inline const VkPipelineLayout native_handle() const { return pipelineLayout_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkPipelineLayout pipelineLayout_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
