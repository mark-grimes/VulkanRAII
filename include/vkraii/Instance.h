#pragma once

#include <vulkan/vulkan.h>
#include <vector>

namespace vkraii
{
	class Instance
	{
	public:
		explicit Instance( std::nullptr_t null );
		Instance( const VkInstanceCreateInfo& createInfo );
		Instance( const Instance& other ) = delete;
		Instance( Instance&& other );
		Instance& operator=( const Instance& other ) = delete;
		Instance& operator=( Instance&& other );
		~Instance();

		/** @brief The number of enabled layers the Instance was created with
		 *
		 * This serves as the size of the array refered to by ppEnabledLayerNames(). */
		uint32_t enabledLayerCount() const;

		/** @brief The enabled layers the instance was created with
		 *
		 * The returned value is the start of an array of size enabledLayerCount(). Useful for putting
		 * in the VkDeviceCreateInfo struct when creating Device objects.
		 *
		 * Note that these strings are copied out of the VkInstanceCreateInfo struct during construction,
		 * so it is safe to use this value after the lifetime of the VkInstanceCreateInfo struct. */
		const char* const* ppEnabledLayerNames() const;

		/** @brief The enabled layers the instance was created with
		 *
		 * Note that these strings are copied out of the VkInstanceCreateInfo struct during construction,
		 * so it is safe to use this value after the lifetime of the VkInstanceCreateInfo struct. */
		const std::vector<const char*>& enabledLayers() const;

		inline VkInstance native_handle() { return instance_; }
		inline const VkInstance native_handle() const { return instance_; }
	protected:
		VkInstance instance_=VK_NULL_HANDLE;
		std::vector<const char*> layers_; ///< @brief A deep copy of the enabled layers
	};
} // end of namespace vkraii
