#pragma once

#include <vector>
#include <vulkan/vulkan.h>
#include "ImageView.h"
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class SwapChain
	{
	public:
		explicit SwapChain( std::nullptr_t null );
		SwapChain( const vkraii::Device& device, const VkSwapchainCreateInfoKHR& createInfo );
		SwapChain( VkDevice device, const VkSwapchainCreateInfoKHR& createInfo );
		SwapChain( const SwapChain& other ) = delete;
		SwapChain( SwapChain&& other );
		SwapChain& operator=( const SwapChain& other ) = delete;
		SwapChain& operator=( SwapChain&& other );
		~SwapChain();
		std::vector<VkImage> getImages() const;
		std::vector<vkraii::ImageView> getImageViews() const;
		inline VkSwapchainKHR native_handle() { return swapChain_; }
		inline const VkSwapchainKHR native_handle() const { return swapChain_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkSwapchainKHR swapChain_=VK_NULL_HANDLE;
		VkFormat format_=VK_FORMAT_UNDEFINED;
	};
} // end of namespace vkraii
