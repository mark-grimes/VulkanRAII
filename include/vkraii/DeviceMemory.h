#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class DeviceMemory
	{
	public:
		explicit DeviceMemory( std::nullptr_t null );
		DeviceMemory( VkDevice device, const VkMemoryAllocateInfo& allocateInfo );
		DeviceMemory( const vkraii::Device& device, const VkMemoryAllocateInfo& allocateInfo );
		DeviceMemory( const DeviceMemory& other ) = delete;
		DeviceMemory( DeviceMemory&& other );
		DeviceMemory& operator=( const DeviceMemory& other ) = delete;
		DeviceMemory& operator=( DeviceMemory&& other );
		~DeviceMemory();
		inline VkDeviceMemory native_handle() { return memory_; }
		inline const VkDeviceMemory native_handle() const { return memory_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkDeviceMemory memory_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
