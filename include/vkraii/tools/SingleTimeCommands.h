#pragma once

#include <vulkan/vulkan.h>
#include "vkraii/CommandBuffers.h"
#include "vkraii/Fence.h"
//
// Forward declarations
//
namespace vkraii
{
	class Device;
	class CommandPool;
}

namespace vkraii
{
	namespace tools
	{
		/** @brief Class to assist in creating one off VkCommandBuffers, e.g. for copying memory
		 * @author Mark Grimes
		 * @date 29/Oct/2018
		 * @copyright Copyright Mark Grimes. All rights reserved.
		 */
		class SingleTimeCommands
		{
		public:
			SingleTimeCommands( const vkraii::Device& device, vkraii::CommandPool& commandPool );
			~SingleTimeCommands();
			VkCommandBuffer commandBuffer();
			void submit( VkQueue queue );
		protected:
			VkDevice device_;
			VkCommandPool commandPool_;
			vkraii::CommandBuffers commandBuffers_;
			vkraii::Fence fence_;
			static vkraii::CommandBuffers createCommandBuffers( const vkraii::Device& device, vkraii::CommandPool& commandPool );
		};

	}  // end of namespace vkraii::tools
} // end of namespace vkraii
