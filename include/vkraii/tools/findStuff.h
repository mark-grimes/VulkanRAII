#pragma once

#include <vulkan/vulkan.h>

namespace vkraii
{
	namespace tools
	{

		uint32_t findMemoryType( VkPhysicalDevice physicalDevice, const VkMemoryPropertyFlags& requiredProperties, uint32_t indexFilter=0xffff, size_t minimumSize=0 );

	}  // end of namespace vkraii::tools
} // end of namespace vkraii
