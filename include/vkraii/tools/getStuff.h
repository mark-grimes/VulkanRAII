#pragma once

#include <vector>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Instance;
}

namespace vkraii
{
	namespace tools
	{
		std::vector<VkFormat> getPhysicalDeviceSupportedFormats( VkPhysicalDevice physicalDevice );
		std::vector<VkQueueFamilyProperties> getAvailableQueueFamilies( VkPhysicalDevice device );
		std::vector<VkExtensionProperties> getDeviceExtensionSupport( VkPhysicalDevice device );
		std::vector<VkPhysicalDevice> getAvailableDevices( const vkraii::Instance& instance );
		std::vector<VkPhysicalDevice> getAvailableDevices( const VkInstance instance );
	}  // end of namespace vkraii::tools
} // end of namespace vkraii
