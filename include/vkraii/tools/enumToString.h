#pragma once

#include <vulkan/vulkan.h>

namespace vkraii
{
	namespace tools
	{
		char const* enumToString( VkResult error );
		char const* enumToString( VkPhysicalDeviceType type );
		char const* enumToString( VkFormat format );
		char const* enumToString( VkColorSpaceKHR mode );
		char const* enumToString( VkPresentModeKHR mode );
	}  // end of namespace vkraii::tools
} // end of namespace vkraii
