#pragma once

#include <iosfwd>
#include <vulkan/vulkan.h>

namespace vkraii
{
	namespace tools
	{
		void printAvailableDevicesInfo( std::ostream& output );
		void printAvailableExtensions( std::ostream& output );
		void printAvailableLayers( std::ostream& output );
		void printAvailableMemoryProperties( std::ostream& output, VkPhysicalDevice device );
		void printPhysicalDeviceFormatProperties( std::ostream& output, VkFormat format, VkPhysicalDevice physicalDevice );
		void printPhysicalDeviceSupportedFormats( std::ostream& output, VkPhysicalDevice physicalDevice );
		void printAvailableQueueFamilies( std::ostream& output, VkPhysicalDevice device );
		void printDeviceExtensionSupport( std::ostream& output, VkPhysicalDevice device );
	}  // end of namespace vkraii::tools
} // end of namespace vkraii
