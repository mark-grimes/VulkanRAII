#pragma once

#include <string>

namespace vkraii
{
	namespace tools
	{
		class RawImage
		{
		public:
			RawImage();
			explicit RawImage( const std::string& path );
			RawImage( const RawImage& other ) = delete;
			RawImage( RawImage&& other );
			RawImage& operator=( const RawImage& other ) = delete;
			RawImage& operator=( RawImage&& other );
			~RawImage();

			int width() const;
			int height() const;
			const void* data() const;
			size_t size() const;
		protected:
			int width_=0;
			int height_=0;
			int channels_=0;
			void* pData_=nullptr;
		};

	}  // end of namespace vkraii::tools
} // end of namespace vkraii
