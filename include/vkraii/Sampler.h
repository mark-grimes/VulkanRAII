#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class Sampler
	{
	public:
		explicit Sampler( std::nullptr_t null );
		Sampler( const vkraii::Device& device, const VkSamplerCreateInfo& createInfo );
		Sampler( const Sampler& other ) = delete;
		Sampler( Sampler&& other );
		Sampler& operator=( const Sampler& other ) = delete;
		Sampler& operator=( Sampler&& other );
		~Sampler();
		inline VkSampler native_handle() { return sampler_; }
		inline const VkSampler native_handle() const { return sampler_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkSampler sampler_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
