#pragma once

#include <vector>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Instance;
}

namespace vkraii
{
	class Surface; // Forward declare so I can set as a friend of SwapChainSupportDetails

	struct SwapChainSupportDetails
	{
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	protected:
		SwapChainSupportDetails( uint32_t formatsCount, uint32_t presentModesCount );
		friend class vkraii::Surface;
	};

	class Surface
	{
	public:
		explicit Surface( std::nullptr_t null );
		Surface( vkraii::Instance& instance, VkSurfaceKHR rawPointer );
		Surface( VkInstance instance, VkSurfaceKHR rawPointer );
		Surface( const Surface& other ) = delete;
		Surface( Surface&& other );
		Surface& operator=( const Surface& other ) = delete;
		Surface& operator=( Surface&& other );
		~Surface();
		bool supportedByDevice( VkPhysicalDevice device, unsigned queueFamilyIndex ) const;
		vkraii::SwapChainSupportDetails swapChainSupportDetails( VkPhysicalDevice device ) const;
		inline VkSurfaceKHR native_handle() { return surface_; }
		inline const VkSurfaceKHR native_handle() const { return surface_; }
	protected:
		VkInstance instance_=VK_NULL_HANDLE;
		VkSurfaceKHR surface_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
