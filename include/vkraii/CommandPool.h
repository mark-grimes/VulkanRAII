#pragma once

#include <cstddef>
#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Device;
}

namespace vkraii
{
	class CommandPool
	{
	public:
		explicit CommandPool( std::nullptr_t null );
		CommandPool( const vkraii::Device& device, const VkCommandPoolCreateInfo& createInfo );
		CommandPool( VkDevice device, const VkCommandPoolCreateInfo& createInfo );
		CommandPool( const CommandPool& other ) = delete;
		CommandPool( CommandPool&& other );
		CommandPool& operator=( const CommandPool& other ) = delete;
		CommandPool& operator=( CommandPool&& other );
		~CommandPool();
		inline VkCommandPool native_handle() { return commandPool_; }
		inline const VkCommandPool native_handle() const { return commandPool_; }
	protected:
		VkDevice device_=VK_NULL_HANDLE;
		VkCommandPool commandPool_=VK_NULL_HANDLE;
	};
} // end of namespace vkraii
