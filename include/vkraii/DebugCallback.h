#pragma once

#include <vulkan/vulkan.h>
//
// Forward declarations
//
namespace vkraii
{
	class Instance;
}

namespace vkraii
{
	class DebugCallback
	{
	public:
		DebugCallback( vkraii::Instance& instance );
		void reInitiate( vkraii::Instance& instance );
		DebugCallback( const DebugCallback& other ) = delete;
		DebugCallback( DebugCallback&& other ) = delete;
		DebugCallback& operator=( const DebugCallback& other ) = delete;
		DebugCallback& operator=( DebugCallback&& other ) = delete;
		~DebugCallback();
	protected:
		VkInstance instance_=nullptr;
		PFN_vkDestroyDebugUtilsMessengerEXT destroyFunc_=nullptr;
		VkDebugUtilsMessengerEXT pCallback_=VK_NULL_HANDLE;
		bool initialisationFinished_=false;
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback_(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData );
	};
} // end of namespace vkraii
