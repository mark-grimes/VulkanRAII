/** @file
 * @brief Main include point for Apple frameworks
 *
 * When using as a macOS or iOS framework, developers expect to use like
 * ```
 * #include <VulkanRAII/VulkanRAII.h>
 * ```
 * This is that file.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com)
 * @date 18/Jan/2019
 * @copyright Copyright Mark Grimes 2019. Licence to be decided.
 */
#include "vkraii/Buffer.h"
#include "vkraii/CommandBuffers.h"
#include "vkraii/CommandPool.h"
#include "vkraii/DebugCallback.h"
#include "vkraii/DescriptorPool.h"
#include "vkraii/DescriptorSet.h"
#include "vkraii/DescriptorSetLayout.h"
#include "vkraii/Device.h"
#include "vkraii/DeviceMemory.h"
#include "vkraii/Fence.h"
#include "vkraii/Framebuffer.h"
#include "vkraii/Image.h"
#include "vkraii/ImageView.h"
#include "vkraii/Instance.h"
#include "vkraii/MappedMemory.h"
#include "vkraii/Pipeline.h"
#include "vkraii/PipelineCache.h"
#include "vkraii/PipelineLayout.h"
#include "vkraii/RenderPass.h"
#include "vkraii/Sampler.h"
#include "vkraii/Semaphore.h"
#include "vkraii/ShaderModule.h"
#include "vkraii/Surface.h"
#include "vkraii/SwapChain.h"
#include "vkraii/tools.h"
#include "vkraii/version.h"
