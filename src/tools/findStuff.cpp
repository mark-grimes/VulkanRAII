#include "vkraii/tools/findStuff.h"
#include <limits>

uint32_t vkraii::tools::findMemoryType( VkPhysicalDevice physicalDevice, const VkMemoryPropertyFlags& requiredProperties, uint32_t indexFilter, size_t minimumSize )
{
	VkPhysicalDeviceMemoryProperties properties;
	vkGetPhysicalDeviceMemoryProperties( physicalDevice, &properties );
	for( size_t index=0; index<properties.memoryTypeCount; ++index )
	{
		const VkMemoryType& memoryType = properties.memoryTypes[index];

		if( (memoryType.propertyFlags & requiredProperties)
			&& (indexFilter & (1<<index))
			&& (properties.memoryHeaps[memoryType.heapIndex].size > minimumSize) )
		{
			return static_cast<uint32_t>(index);
		}
	}

	// If control got this far than no suitable memory type was found
	return std::numeric_limits<uint32_t>::max();
}
