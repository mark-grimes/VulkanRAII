#include "vkraii/tools/printStuff.h"
#include <iostream>
#include <iomanip>
#include "vkraii/Instance.h"
#include "vkraii/tools/getStuff.h"
#include "vkraii/tools/enumToString.h"

// Use the unnamed namespace for things only in this file
namespace
{
	char const* deviceTypeString( const VkPhysicalDeviceType type )
	{
		switch( type )
		{
			case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "OTHER";
			case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "INTEGRATED_GPU";
			case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "DISCRETE_GPU";
			case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "VIRTUAL_GPU";
			case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
			default: return "<unknown>";
		}
	} // end of function deviceTypeString

	/** @brief Reverts the fill character of the stream on destruction */
	class SetFillSentry
	{
	public:
		SetFillSentry( std::ostream& stream, char newFill )
			: stream_( stream ), oldFill_( stream_.fill() ) { stream_.fill( newFill ); }
		~SetFillSentry() { stream_.fill( oldFill_ ); }
	protected:
		std::ostream& stream_;
		char oldFill_;
	};
} // end of the unnamed namespace

void vkraii::tools::printAvailableDevicesInfo( std::ostream& output )
{
	SetFillSentry setFill( output, '0' ); // Fill for the hex output, i.e. "0x01" instead of "0x 1"

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Not set";
	appInfo.applicationVersion = 0;
	appInfo.pEngineName = "VulkanInferencePrintInfo";
	appInfo.engineVersion = 0;
	appInfo.apiVersion = VK_API_VERSION_1_0;

	std::vector<char const*> requiredExtensions{"VK_KHR_get_physical_device_properties2"};
	std::vector<char const*> requiredLayers;

	VkInstanceCreateInfo createInfo={};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
	createInfo.ppEnabledExtensionNames = requiredExtensions.data();
	createInfo.enabledLayerCount = static_cast<uint32_t>(requiredLayers.size());
	createInfo.ppEnabledLayerNames = requiredLayers.data();

	vkraii::Instance instance( createInfo );

	for( auto& physicalDevice : vkraii::tools::getAvailableDevices(instance) )
	{
		VkPhysicalDeviceSubgroupProperties subgroupProperties = {};
		subgroupProperties.sType=VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES;
		VkPhysicalDeviceProperties2 physicalDeviceProperties = {};
		physicalDeviceProperties.sType=VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
		physicalDeviceProperties.pNext=&subgroupProperties;
		VkPhysicalDeviceProperties& deviceProperties=physicalDeviceProperties.properties;

		vkGetPhysicalDeviceProperties( physicalDevice, &deviceProperties );

		// // TEMPORARY!!! - Don't use vkGetPhysicalDeviceProperties2 because it doesn't link on MoltenVK
		// if( deviceProperties.apiVersion>=VK_API_VERSION_1_1 ) vkGetPhysicalDeviceProperties2( physicalDevice, &physicalDeviceProperties );
		// else
		{
			PFN_vkGetPhysicalDeviceProperties2KHR getPropertiesFunc=(PFN_vkGetPhysicalDeviceProperties2KHR) vkGetInstanceProcAddr(instance.native_handle(), "vkGetPhysicalDeviceProperties2KHR");
			if( getPropertiesFunc!=nullptr ) getPropertiesFunc( physicalDevice, &physicalDeviceProperties );
			// if getPropertiesFunc was null, can't do anything so have to ignore the subgroupProperties
		}

		const uint32_t api=deviceProperties.apiVersion;
		output << "Physical device " << deviceTypeString(deviceProperties.deviceType) << " '" << deviceProperties.deviceName << "', driver v" << deviceProperties.driverVersion << ", API v" << VK_VERSION_MAJOR(api) << "." << VK_VERSION_MINOR(api) << "." << VK_VERSION_PATCH(api) << "\n";
		output << "\tmaxImageDimension3D            = " << deviceProperties.limits.maxImageDimension3D << "\n";
		output << "\tmaxComputeSharedMemorySize     = " << deviceProperties.limits.maxComputeSharedMemorySize << "\n";
		output << "\tmaxComputeWorkGroupCount       = [" << deviceProperties.limits.maxComputeWorkGroupCount[0] << "," << deviceProperties.limits.maxComputeWorkGroupCount[1] << "," << deviceProperties.limits.maxComputeWorkGroupCount[2] << "]\n";
		output << "\tmaxComputeWorkGroupInvocations = " << deviceProperties.limits.maxComputeWorkGroupInvocations << "\n";
		output << "\tmaxComputeWorkGroupSize        = [" << deviceProperties.limits.maxComputeWorkGroupSize[0] << "," << deviceProperties.limits.maxComputeWorkGroupSize[1] << "," << deviceProperties.limits.maxComputeWorkGroupSize[2] << "]\n";
		output << "\tSubgroup properties:\n"
			<< "\t\tsubgroupSize                   = " << subgroupProperties.subgroupSize << "\n"
			<< "\t\tsupportedStages                = " << subgroupProperties.supportedStages << "\n"
			<< "\t\tsupportedOperations            = " << subgroupProperties.supportedOperations << "\n"
			<< "\t\tquadOperationsInAllStages      = " << subgroupProperties.quadOperationsInAllStages << "\n";


		const auto queueFamilies=vkraii::tools::getAvailableQueueFamilies(physicalDevice);

		output << "\tQueues:\n";
		for( int index=0; index<queueFamilies.size(); ++index )
		{
			output << "\t\tQueue family " << index << ": queueCount=" << queueFamilies[index].queueCount << ", "
				<< (queueFamilies[index].queueFlags & VK_QUEUE_GRAPHICS_BIT ? " GRAPHICS_BIT" : "")
				<< (queueFamilies[index].queueFlags & VK_QUEUE_COMPUTE_BIT ? " COMPUTE_BIT" : "")
				<< (queueFamilies[index].queueFlags & VK_QUEUE_TRANSFER_BIT ? " TRANSFER_BIT" : "")
				<< (queueFamilies[index].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT ? " SPARSE_BINDING_BIT" : "")
				<< (queueFamilies[index].queueFlags & VK_QUEUE_PROTECTED_BIT ? " PROTECTED_BIT" : "") << "\n";
		}

		// Need to have shaderStorageImageExtendedFormats to be able to use 3 dimensional images
		VkPhysicalDeviceFeatures features;
		vkGetPhysicalDeviceFeatures( physicalDevice, &features );

		VkPhysicalDeviceMemoryProperties properties;
		vkGetPhysicalDeviceMemoryProperties( physicalDevice, &properties );
		output << "\tDevice has " << properties.memoryTypeCount << " memory types:\n";
		for( size_t index=0; index<properties.memoryTypeCount; ++index )
		{
			output << "\t\tHeap [" << properties.memoryTypes[index].heapIndex << "] - (0x" << std::hex << std::setw(2) << properties.memoryTypes[index].propertyFlags << std::dec << ")"
				<< (VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT & properties.memoryTypes[index].propertyFlags ? " DEVICE_LOCAL" : "" )
				<< (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_VISIBLE" : "" )
				<< (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_COHERENT" : "" )
				<< (VK_MEMORY_PROPERTY_HOST_CACHED_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_CACHED" : "" )
				<< (VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT & properties.memoryTypes[index].propertyFlags ? " LAZILY_ALLOCATED" : "" )
				<< (VK_MEMORY_PROPERTY_PROTECTED_BIT & properties.memoryTypes[index].propertyFlags ? " PROTECTED" : "" ) << "\n";
		}

		output << "\tDevice has " << properties.memoryHeapCount << " heaps:\n";
		for( size_t index=0; index<properties.memoryHeapCount; ++index )
		{
			output << "\t\t[" << index << "] - Size " << properties.memoryHeaps[index].size
				<< ", flags (0x" << std::setw(2) << std::hex << properties.memoryHeaps[index].flags << std::dec << ")"
				<< (VK_MEMORY_HEAP_DEVICE_LOCAL_BIT & properties.memoryHeaps[index].flags ? " DEVICE_LOCAL" : "" )
				<< (VK_MEMORY_HEAP_MULTI_INSTANCE_BIT & properties.memoryHeaps[index].flags ? " MULTI_INSTANCE" : "" ) << "\n";
		} // end of loop over heaps
	} // end of loop over physical devices
} // end of function vkraii::tools::printAvailableDevicesInfo

void vkraii::tools::printAvailableExtensions( std::ostream& output )
{
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
	std::vector<VkExtensionProperties> extensions(extensionCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
	output << "available extensions:" << std::endl;

	for (const auto& extension : extensions)
	{
		output << "\t" << extension.extensionName << std::endl;
	}
}

void vkraii::tools::printAvailableLayers( std::ostream& output )
{
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties( &layerCount, nullptr );
	std::vector<VkLayerProperties> layers(layerCount);
	vkEnumerateInstanceLayerProperties( &layerCount, layers.data() );
	output << "available layers:" << std::endl;

	for (const auto& layer : layers)
	{
		output << "\t" << layer.layerName << std::endl;
	}
}

void vkraii::tools::printAvailableMemoryProperties( std::ostream& output, VkPhysicalDevice physicalDevice )
{
	VkPhysicalDeviceMemoryProperties properties;
	vkGetPhysicalDeviceMemoryProperties( physicalDevice, &properties );
	output << "Device has " << properties.memoryTypeCount << " memory types:\n";
	for( size_t index=0; index<properties.memoryTypeCount; ++index )
	{
		output << "\tHeap [" << properties.memoryTypes[index].heapIndex << "] -"
			<< (VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT & properties.memoryTypes[index].propertyFlags ? " DEVICE_LOCAL" : "" )
			<< (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_VISIBLE" : "" )
			<< (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_COHERENT" : "" )
			<< (VK_MEMORY_PROPERTY_HOST_CACHED_BIT & properties.memoryTypes[index].propertyFlags ? " HOST_CACHED" : "" )
			<< (VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT & properties.memoryTypes[index].propertyFlags ? " LAZILY_ALLOCATED" : "" )
			<< (VK_MEMORY_PROPERTY_PROTECTED_BIT & properties.memoryTypes[index].propertyFlags ? " PROTECTED" : "" )
			<< "\n";
	}
	output << "Device has " << properties.memoryHeapCount << " heaps:\n";
	for( size_t index=0; index<properties.memoryHeapCount; ++index )
	{
		output << "\t[" << index << "] - Size "<< properties.memoryHeaps[index].size << ", flags"
			<< (VK_MEMORY_HEAP_DEVICE_LOCAL_BIT & properties.memoryHeaps[index].flags ? " DEVICE_LOCAL" : "" )
			<< (VK_MEMORY_HEAP_MULTI_INSTANCE_BIT & properties.memoryHeaps[index].flags ? " MULTI_INSTANCE" : "" )
			<< "\n";
	}
}

void vkraii::tools::printPhysicalDeviceFormatProperties( std::ostream& output, VkFormat format, VkPhysicalDevice physicalDevice )
{
	VkFormatProperties properties;
	vkGetPhysicalDeviceFormatProperties( physicalDevice, format, &properties );
	auto printFeatures=[]( std::ostream& output, const VkFormatFeatureFlags& flags )
		{
			output << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT ? " SAMPLED_IMAGE_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT ? " STORAGE_IMAGE_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT ? " STORAGE_IMAGE_ATOMIC_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT ? " UNIFORM_TEXEL_BUFFER_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT ? " STORAGE_TEXEL_BUFFER_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT ? " STORAGE_TEXEL_BUFFER_ATOMIC_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT ? " VERTEX_BUFFER_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT ? " COLOR_ATTACHMENT_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT ? " COLOR_ATTACHMENT_BLEND_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT ? " DEPTH_STENCIL_ATTACHMENT_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_BLIT_SRC_BIT ? " BLIT_SRC_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_BLIT_DST_BIT ? " BLIT_DST_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT ? " SAMPLED_IMAGE_FILTER_LINEAR_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_TRANSFER_SRC_BIT ? " TRANSFER_SRC_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_TRANSFER_DST_BIT ? " TRANSFER_DST_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT ? " MIDPOINT_CHROMA_SAMPLES_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT ? " SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT ? " SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT ? " SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT ? " SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_DISJOINT_BIT ? " DISJOINT_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT ? " COSITED_CHROMA_SAMPLES_BIT" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG ? " SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG" : "")
			       << (flags & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT ? " SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT" : "");
		};

	output << "Features for format " << vkraii::tools::enumToString(format) << "\n\tlinearTilingFeatures:";
	printFeatures( output, properties.linearTilingFeatures );
	output << "\n\toptimalTilingFeatures:";
	printFeatures( output, properties.optimalTilingFeatures );
	output << "\n\tbufferFeatures:";
	printFeatures( output, properties.bufferFeatures );
	output << "\n";
}

void vkraii::tools::printPhysicalDeviceSupportedFormats( std::ostream& output, VkPhysicalDevice physicalDevice )
{
	output << "Supported device formats:\n";
	for( const auto& format : getPhysicalDeviceSupportedFormats( physicalDevice ) )
	{
		output << "\t" << vkraii::tools::enumToString(format) << "\n";
	}
}

void vkraii::tools::printAvailableQueueFamilies( std::ostream& output, VkPhysicalDevice device )
{
	output << "available queues:" << std::endl;

	for( const auto& queue : vkraii::tools::getAvailableQueueFamilies(device) )
	{
		output << "\t" << queue.queueCount << " queues, supporting"
			<< (queue.queueFlags & VK_QUEUE_GRAPHICS_BIT ? " GRAPHICS" : "")
			<< (queue.queueFlags & VK_QUEUE_COMPUTE_BIT ? " COMPUTE" : "")
			<< (queue.queueFlags & VK_QUEUE_TRANSFER_BIT ? " TRANSFER" : "")
			<< (queue.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT ? " SPARSE_BINDING" : "")
			<< (queue.queueFlags & VK_QUEUE_PROTECTED_BIT ? " PROTECTED" : "")
			<< std::endl;
	}
}

void vkraii::tools::printDeviceExtensionSupport( std::ostream& output, VkPhysicalDevice device )
{
	output << "available extensions:" << std::endl;

	for( const auto& extension : vkraii::tools::getDeviceExtensionSupport(device) )
	{
		output << "\t" << extension.extensionName
			<< " " << extension.specVersion
			<< std::endl;
	}
}
