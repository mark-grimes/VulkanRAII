#include "vkraii/tools/SingleTimeCommands.h"
#include "vkraii/Device.h"
#include "vkraii/CommandPool.h"

vkraii::tools::SingleTimeCommands::SingleTimeCommands( const vkraii::Device& device, vkraii::CommandPool& commandPool )
	: device_( device.native_handle() ),
	  commandPool_( commandPool.native_handle() ),
	  commandBuffers_( createCommandBuffers(device,commandPool) ),
	  fence_( device )
{
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer( commandBuffers_.native_handle(), &beginInfo );
}

vkraii::tools::SingleTimeCommands::~SingleTimeCommands()
{
	// Make sure all the commands have finished before allowing the object to be destructed
	fence_.wait();
}

VkCommandBuffer vkraii::tools::SingleTimeCommands::commandBuffer()
{
	return commandBuffers_.native_handle();
}

void vkraii::tools::SingleTimeCommands::submit( VkQueue queue )
{
	vkEndCommandBuffer( commandBuffers_.native_handle() );

	VkCommandBuffer commandBufferHandles[]={ commandBuffers_.native_handle() };
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = commandBufferHandles;

	fence_.reset();
	vkQueueSubmit( queue, 1, &submitInfo, fence_.native_handle() );
}

vkraii::CommandBuffers vkraii::tools::SingleTimeCommands::createCommandBuffers( const vkraii::Device& device, vkraii::CommandPool& commandPool )
{
	VkCommandBufferAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocateInfo.commandPool = commandPool.native_handle();
	allocateInfo.commandBufferCount = 1;
	return { device, allocateInfo };
}
