#include "vkraii/tools/RawImage.h"
#include <stb_image.h> // From <project root>/external/stb. Required for RawImage implementation.

vkraii::tools::RawImage::RawImage()
{
	// No operation, members have initialisation values in the header
}

vkraii::tools::RawImage::RawImage( const std::string& path )
	: pData_( stbi_load(path.c_str(),&width_,&height_,&channels_,STBI_rgb_alpha) )
{
	channels_=4; // STBI_rgb_alpha forces channels to be 4
}

vkraii::tools::RawImage::RawImage( RawImage&& other )
	: width_( other.width_ ), height_( other.height_ ), channels_( other.channels_ ), pData_( other.pData_ )
{
	other.width_=0;
	other.height_=0;
	other.channels_=0;
	other.pData_=nullptr;
}

vkraii::tools::RawImage& vkraii::tools::RawImage::operator=( RawImage&& other )
{
	if( this!=&other )
	{
		width_=other.width_;
		height_=other.height_;
		channels_=other.channels_;
		pData_=other.pData_;
		other.width_=0;
		other.height_=0;
		other.channels_=0;
		other.pData_=nullptr;
	}
	return *this;
}

vkraii::tools::RawImage::~RawImage()
{
	if( pData_!=nullptr ) stbi_image_free( pData_ );
}

int vkraii::tools::RawImage::width() const
{
	return width_;
}

int vkraii::tools::RawImage::height() const
{
	return height_;
}

const void* vkraii::tools::RawImage::data() const
{
	return pData_;
}

size_t vkraii::tools::RawImage::size() const
{
	return width_*height_*channels_;
}
