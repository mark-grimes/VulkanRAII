#include "vkraii/SwapChain.h"
#include <string>
#include <stdexcept>
#include "vkraii/Device.h"
#include "vkraii/ImageView.h"
#include "vkraii/tools/enumToString.h"

vkraii::SwapChain::SwapChain( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::SwapChain::SwapChain( const vkraii::Device& device, const VkSwapchainCreateInfoKHR& createInfo )
	: SwapChain(device.native_handle(),createInfo)
{
	// No operation besides the initialiser list
}

vkraii::SwapChain::SwapChain( VkDevice device, const VkSwapchainCreateInfoKHR& createInfo )
	: device_(device)
{
	format_=createInfo.imageFormat;
	VkResult result = vkCreateSwapchainKHR( device_, &createInfo, nullptr, &swapChain_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create swap chain: ")+vkraii::tools::enumToString(result) );
}

vkraii::SwapChain::SwapChain( SwapChain&& other )
	: device_(other.device_),
	  swapChain_(other.swapChain_),
	  format_(other.format_)
{
	// Make sure the other instance doesn't call the destroy function when it destructs
	other.device_=VK_NULL_HANDLE;
	other.swapChain_=VK_NULL_HANDLE;
}

vkraii::SwapChain& vkraii::SwapChain::operator=( SwapChain&& other )
{
	if( this!=&other )
	{
		if( swapChain_!=VK_NULL_HANDLE ) vkDestroySwapchainKHR( device_, swapChain_, nullptr );
		device_=other.device_;
		swapChain_=other.swapChain_;
		format_=other.format_;
		other.device_=VK_NULL_HANDLE;
		other.swapChain_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::SwapChain::~SwapChain()
{
	if( swapChain_!=VK_NULL_HANDLE ) vkDestroySwapchainKHR( device_, swapChain_, nullptr );
}

std::vector<VkImage> vkraii::SwapChain::getImages() const
{
	uint32_t imageCount;
	vkGetSwapchainImagesKHR( device_, swapChain_, &imageCount, nullptr );
	std::vector<VkImage> result(imageCount);
	vkGetSwapchainImagesKHR( device_, swapChain_, &imageCount, result.data() );
	return result;
}

std::vector<vkraii::ImageView> vkraii::SwapChain::getImageViews() const
{
	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	createInfo.format = format_;
	createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	createInfo.subresourceRange.baseMipLevel = 0;
	createInfo.subresourceRange.levelCount = 1;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.layerCount = 1;

	std::vector<vkraii::ImageView> imageViews;
	for( const auto& image : getImages() )
	{
		createInfo.image = image;
		imageViews.emplace_back( device_, createInfo );
	}

	return imageViews;
}
