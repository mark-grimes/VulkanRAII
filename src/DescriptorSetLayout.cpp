#include "vkraii/DescriptorSetLayout.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::DescriptorSetLayout::DescriptorSetLayout( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::DescriptorSetLayout::DescriptorSetLayout( const vkraii::Device& device, const VkDescriptorSetLayoutCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateDescriptorSetLayout( device_, &createInfo, nullptr, &descriptorSetLayout_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't allocate memory: ")+vkraii::tools::enumToString(result) );
}

vkraii::DescriptorSetLayout::DescriptorSetLayout( DescriptorSetLayout&& other )
	: device_( other.device_ ),
	  descriptorSetLayout_( other.descriptorSetLayout_ )
{
	other.device_=VK_NULL_HANDLE;
	other.descriptorSetLayout_=VK_NULL_HANDLE;
}

vkraii::DescriptorSetLayout& vkraii::DescriptorSetLayout::operator=( DescriptorSetLayout&& other )
{
	if( this!=&other )
	{
		if( descriptorSetLayout_!=VK_NULL_HANDLE ) vkDestroyDescriptorSetLayout( device_, descriptorSetLayout_, nullptr );
		device_=other.device_;
		descriptorSetLayout_=other.descriptorSetLayout_;
		other.device_=VK_NULL_HANDLE;
		other.descriptorSetLayout_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::DescriptorSetLayout::~DescriptorSetLayout()
{
	if( descriptorSetLayout_!=VK_NULL_HANDLE ) vkDestroyDescriptorSetLayout( device_, descriptorSetLayout_, nullptr );
}
