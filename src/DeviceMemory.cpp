#include "vkraii/DeviceMemory.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::DeviceMemory::DeviceMemory( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::DeviceMemory::DeviceMemory( const vkraii::Device& device, const VkMemoryAllocateInfo& allocateInfo )
	: DeviceMemory( device.native_handle(), allocateInfo )
{
}

vkraii::DeviceMemory::DeviceMemory( VkDevice device, const VkMemoryAllocateInfo& allocateInfo )
	: device_( device )
{
	VkResult result = vkAllocateMemory( device_, &allocateInfo, nullptr, &memory_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't allocate memory: ")+vkraii::tools::enumToString(result) );
}

vkraii::DeviceMemory::DeviceMemory( DeviceMemory&& other )
	: device_( other.device_ ),
	  memory_( other.memory_ )
{
	other.device_=VK_NULL_HANDLE;
	other.memory_=VK_NULL_HANDLE;
}

vkraii::DeviceMemory& vkraii::DeviceMemory::operator=( DeviceMemory&& other )
{
	if( this!=&other )
	{
		if( memory_!=VK_NULL_HANDLE ) vkFreeMemory( device_, memory_, nullptr );
		device_=other.device_;
		memory_=other.memory_;
		other.device_=VK_NULL_HANDLE;
		other.memory_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::DeviceMemory::~DeviceMemory()
{
	if( memory_!=VK_NULL_HANDLE ) vkFreeMemory( device_, memory_, nullptr );
}
