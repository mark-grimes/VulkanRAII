#include "vkraii/DescriptorSet.h"
#include <stdexcept>
#include <string>
#include <array>
#include "vkraii/Device.h"
#include "vkraii/DescriptorPool.h"
#include "vkraii/DescriptorSetLayout.h"
#include "vkraii/tools/enumToString.h"

vkraii::DescriptorSet::DescriptorSet( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::DescriptorSet::DescriptorSet( const vkraii::Device& device, const vkraii::DescriptorPool& descriptorPool, const vkraii::DescriptorSetLayout& descriptorSetLayout )
	: device_( device.native_handle() ),
	  descriptorPool_( descriptorPool.native_handle() )
{
	std::array<VkDescriptorSetLayout,1> setLayouts{ descriptorSetLayout.native_handle() };
	VkDescriptorSetAllocateInfo allocateInfo={};
	allocateInfo.sType=VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocateInfo.descriptorPool=descriptorPool_;
	allocateInfo.descriptorSetCount=setLayouts.size();
	allocateInfo.pSetLayouts=setLayouts.data();

	VkResult result = vkAllocateDescriptorSets( device_, &allocateInfo, &descriptorSet_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't allocate descriptor set: ")+vkraii::tools::enumToString(result) );

	// Check if the pool allows freeing, if not set set descriptorPool_ to nullptr as a signal not
	// to free the descriptor set
	if( !descriptorPool.freeDescriptorSetBit() ) descriptorPool_=VK_NULL_HANDLE;
}

vkraii::DescriptorSet::DescriptorSet( DescriptorSet&& other )
	: device_( other.device_ ),
	  descriptorPool_( other.descriptorPool_ ),
	  descriptorSet_( other.descriptorSet_ )
{
	other.device_=VK_NULL_HANDLE;
	other.descriptorPool_=VK_NULL_HANDLE;
	other.descriptorSet_=VK_NULL_HANDLE;
}

vkraii::DescriptorSet& vkraii::DescriptorSet::operator=( DescriptorSet&& other )
{
	if( this!=&other )
	{
		// If descriptorPool_ is null, that means the constructor realised it didn't have
		// VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT set so the descriptor set should not be
		// freed.
		if( descriptorSet_!=VK_NULL_HANDLE && descriptorPool_!=VK_NULL_HANDLE ) vkFreeDescriptorSets( device_, descriptorPool_, 1, &descriptorSet_ );
		device_=other.device_;
		descriptorPool_=other.descriptorPool_;
		descriptorSet_=other.descriptorSet_;
		other.device_=VK_NULL_HANDLE;
		other.descriptorPool_=VK_NULL_HANDLE;
		other.descriptorSet_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::DescriptorSet::~DescriptorSet()
{
	// If descriptorPool_ is null, that means the constructor realised it didn't have
	// VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT set so the descriptor set should not be
	// freed.
	if( descriptorSet_!=VK_NULL_HANDLE && descriptorPool_!=VK_NULL_HANDLE ) vkFreeDescriptorSets( device_, descriptorPool_, 1, &descriptorSet_ );
}
