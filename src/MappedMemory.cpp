#include "vkraii/MappedMemory.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/DeviceMemory.h"
#include "vkraii/tools/enumToString.h"

vkraii::MappedMemory::MappedMemory( const vkraii::Device& device, vkraii::DeviceMemory& memory, VkDeviceSize size, size_t offset )
	: device_(device.native_handle()),
	  memory_(memory.native_handle())
{
	VkResult result=vkMapMemory( device_, memory_, offset, size, 0, &pData_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't map memory: ")+vkraii::tools::enumToString(result) );
}

vkraii::MappedMemory::MappedMemory( MappedMemory&& other )
	: device_( other.device_ ),
	  memory_( other.memory_ ),
	  pData_( other.pData_ )
{
	other.device_=VK_NULL_HANDLE;
	other.memory_=VK_NULL_HANDLE;
	other.pData_=nullptr;
}

vkraii::MappedMemory& vkraii::MappedMemory::operator=( MappedMemory&& other )
{
	if( this!=&other )
	{
		if( memory_!=VK_NULL_HANDLE ) vkUnmapMemory( device_, memory_ );
		device_=other.device_;
		memory_=other.memory_;
		pData_=other.pData_;
		other.device_=VK_NULL_HANDLE;
		other.memory_=VK_NULL_HANDLE;
		other.pData_=nullptr;
	}
	return *this;
}

vkraii::MappedMemory::~MappedMemory()
{
	if( memory_!=VK_NULL_HANDLE ) vkUnmapMemory( device_, memory_ );
}

void vkraii::MappedMemory::unmap()
{
	if( memory_!=VK_NULL_HANDLE ) vkUnmapMemory( device_, memory_ );
	device_=VK_NULL_HANDLE;
	memory_=VK_NULL_HANDLE;
	pData_=nullptr;
}
