#include "vkraii/PipelineLayout.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::PipelineLayout::PipelineLayout( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::PipelineLayout::PipelineLayout( const vkraii::Device& device )
	: device_(device.native_handle())
{
	VkPipelineLayoutCreateInfo createInfo={};
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	createInfo.setLayoutCount = 0; // Optional
	createInfo.pSetLayouts = nullptr; // Optional
	createInfo.pushConstantRangeCount = 0; // Optional
	createInfo.pPushConstantRanges = nullptr; // Optional

	VkResult result = vkCreatePipelineLayout( device_, &createInfo, nullptr, &pipelineLayout_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create PipelineLayout: ")+vkraii::tools::enumToString(result) );
}

vkraii::PipelineLayout::PipelineLayout( const vkraii::Device& device, const VkPipelineLayoutCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreatePipelineLayout( device_, &createInfo, nullptr, &pipelineLayout_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create PipelineLayout: ")+vkraii::tools::enumToString(result) );
}

vkraii::PipelineLayout::PipelineLayout( PipelineLayout&& other )
	: device_( other.device_ ),
	  pipelineLayout_( other.pipelineLayout_ )
{
	other.device_=VK_NULL_HANDLE;
	other.pipelineLayout_=VK_NULL_HANDLE;
}

vkraii::PipelineLayout& vkraii::PipelineLayout::operator=( PipelineLayout&& other )
{
	if( this!=&other )
	{
		if( pipelineLayout_!=VK_NULL_HANDLE ) vkDestroyPipelineLayout( device_, pipelineLayout_, nullptr );
		device_=other.device_;
		pipelineLayout_=other.pipelineLayout_;
		other.device_=VK_NULL_HANDLE;
		other.pipelineLayout_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::PipelineLayout::~PipelineLayout()
{
	if( pipelineLayout_!=VK_NULL_HANDLE ) vkDestroyPipelineLayout( device_, pipelineLayout_, nullptr );
}
