#include "vkraii/CommandBuffers.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::CommandBuffers::CommandBuffers( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::CommandBuffers::CommandBuffers( const vkraii::Device& device, const VkCommandBufferAllocateInfo& allocateInfo )
	: device_(device.native_handle()),
	  commandPool_(allocateInfo.commandPool),
	  commandBuffers_(allocateInfo.commandBufferCount)
{
	VkResult result = vkAllocateCommandBuffers( device_, &allocateInfo, commandBuffers_.data() );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't allocate memory: ")+vkraii::tools::enumToString(result) );
}

vkraii::CommandBuffers::CommandBuffers( CommandBuffers&& other )
	: device_( other.device_ ),
	  commandPool_( other.commandPool_ ),
	  commandBuffers_( std::move(other.commandBuffers_) )
{
	other.device_=VK_NULL_HANDLE;
	other.commandPool_=VK_NULL_HANDLE;
	other.commandBuffers_.clear();
}

vkraii::CommandBuffers& vkraii::CommandBuffers::operator=( CommandBuffers&& other )
{
	if( this!=&other )
	{
		if( !commandBuffers_.empty() ) vkFreeCommandBuffers( device_, commandPool_, static_cast<uint32_t>(commandBuffers_.size()), commandBuffers_.data() );
		device_=other.device_;
		commandPool_=other.commandPool_;
		commandBuffers_=std::move(other.commandBuffers_);
		other.device_=VK_NULL_HANDLE;
		other.commandPool_=VK_NULL_HANDLE;
		other.commandBuffers_.clear();
	}
	return *this;
}

vkraii::CommandBuffers::~CommandBuffers()
{
	if( !commandBuffers_.empty() ) vkFreeCommandBuffers( device_, commandPool_, static_cast<uint32_t>(commandBuffers_.size()), commandBuffers_.data() );
}
