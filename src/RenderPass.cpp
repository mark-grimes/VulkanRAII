#include "vkraii/RenderPass.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::RenderPass::RenderPass( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::RenderPass::RenderPass( const vkraii::Device& device, const VkRenderPassCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateRenderPass( device_, &createInfo, nullptr, &renderPass_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create render pass: ")+vkraii::tools::enumToString(result) );
}

vkraii::RenderPass::RenderPass( RenderPass&& other )
	: device_( other.device_ ),
	  renderPass_( other.renderPass_ )
{
	other.device_=VK_NULL_HANDLE;
	other.renderPass_=VK_NULL_HANDLE;
}

vkraii::RenderPass& vkraii::RenderPass::operator=( RenderPass&& other )
{
	if( this!=&other )
	{
		if( renderPass_!=VK_NULL_HANDLE ) vkDestroyRenderPass( device_, renderPass_, nullptr );
		device_=other.device_;
		renderPass_=other.renderPass_;
		other.device_=VK_NULL_HANDLE;
		other.renderPass_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::RenderPass::~RenderPass()
{
	if( renderPass_!=VK_NULL_HANDLE ) vkDestroyRenderPass( device_, renderPass_, nullptr );
}
