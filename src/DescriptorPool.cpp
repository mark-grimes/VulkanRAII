#include "vkraii/DescriptorPool.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::DescriptorPool::DescriptorPool( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::DescriptorPool::DescriptorPool( const vkraii::Device& device, const VkDescriptorPoolCreateInfo& createInfo )
	: device_(device.native_handle()),
	  freeDescriptorSetBit_( createInfo.flags & VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT )
{
	VkResult result = vkCreateDescriptorPool( device_, &createInfo, nullptr, &descriptorPool_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create descriptor pool: ")+vkraii::tools::enumToString(result) );
}

vkraii::DescriptorPool::DescriptorPool( DescriptorPool&& other )
	: device_( other.device_ ),
	  descriptorPool_( other.descriptorPool_ ),
	  freeDescriptorSetBit_( other.freeDescriptorSetBit_ )
{
	other.device_=VK_NULL_HANDLE;
	other.descriptorPool_=VK_NULL_HANDLE;
}

vkraii::DescriptorPool& vkraii::DescriptorPool::operator=( DescriptorPool&& other )
{
	if( this!=&other )
	{
		if( descriptorPool_!=VK_NULL_HANDLE ) vkDestroyDescriptorPool( device_, descriptorPool_, nullptr );
		device_=other.device_;
		descriptorPool_=other.descriptorPool_;
		freeDescriptorSetBit_=other.freeDescriptorSetBit_;
		other.device_=VK_NULL_HANDLE;
		other.descriptorPool_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::DescriptorPool::~DescriptorPool()
{
	if( descriptorPool_!=VK_NULL_HANDLE ) vkDestroyDescriptorPool( device_, descriptorPool_, nullptr );
}

bool vkraii::DescriptorPool::freeDescriptorSetBit() const
{
	return freeDescriptorSetBit_;
}
