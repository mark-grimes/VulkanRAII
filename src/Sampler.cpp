#include "vkraii/Sampler.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::Sampler::Sampler( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Sampler::Sampler( const vkraii::Device& device, const VkSamplerCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateSampler( device_, &createInfo, nullptr, &sampler_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create sampler: ")+vkraii::tools::enumToString(result) );
}

vkraii::Sampler::Sampler( Sampler&& other )
	: device_( other.device_ ),
	  sampler_( other.sampler_ )
{
	other.device_=VK_NULL_HANDLE;
	other.sampler_=VK_NULL_HANDLE;
}

vkraii::Sampler& vkraii::Sampler::operator=( Sampler&& other )
{
	if( this!=&other )
	{
		if( sampler_!=VK_NULL_HANDLE ) vkDestroySampler( device_, sampler_, nullptr );
		device_=other.device_;
		sampler_=other.sampler_;
		other.device_=VK_NULL_HANDLE;
		other.sampler_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Sampler::~Sampler()
{
	if( sampler_!=VK_NULL_HANDLE ) vkDestroySampler( device_, sampler_, nullptr );
}
