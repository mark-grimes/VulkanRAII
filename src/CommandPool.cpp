#include "vkraii/CommandPool.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::CommandPool::CommandPool( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::CommandPool::CommandPool( const vkraii::Device& device, const VkCommandPoolCreateInfo& createInfo )
	: CommandPool(device.native_handle(),createInfo)
{
	// No operation besides the initialiser list
}

vkraii::CommandPool::CommandPool( VkDevice device, const VkCommandPoolCreateInfo& createInfo )
	: device_(device)
{
	VkResult result = vkCreateCommandPool( device_, &createInfo, nullptr, &commandPool_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create image view: ")+vkraii::tools::enumToString(result) );
}

vkraii::CommandPool::CommandPool( CommandPool&& other )
	: device_(other.device_),
	  commandPool_(other.commandPool_)
{
	// Make sure the other instance doesn't call the destroy function when it destructs
	other.device_=VK_NULL_HANDLE;
	other.commandPool_=VK_NULL_HANDLE;
}

vkraii::CommandPool& vkraii::CommandPool::operator=( CommandPool&& other )
{
	if( this!=&other )
	{
		if( commandPool_!=VK_NULL_HANDLE ) vkDestroyCommandPool(device_,commandPool_,nullptr);
		device_=other.device_;
		commandPool_=other.commandPool_;
		// Make sure the other instance doesn't call the destroy function when it destructs
		other.device_=VK_NULL_HANDLE;
		other.commandPool_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::CommandPool::~CommandPool()
{
	if( commandPool_!=VK_NULL_HANDLE ) vkDestroyCommandPool(device_,commandPool_,nullptr);
}
