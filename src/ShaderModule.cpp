#include "vkraii/ShaderModule.h"
#include <stdexcept>
#include <fstream>
#include <vector>
#include "vkraii/tools/enumToString.h"
#include "vkraii/Device.h"

vkraii::ShaderModule::ShaderModule( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::ShaderModule::ShaderModule( const vkraii::Device& device, const std::string& filename, VkShaderStageFlagBits shaderStage )
	: device_( device.native_handle() )
{
	std::ifstream input( filename, std::ios::ate | std::ios::binary );
	if( !input.is_open() ) throw std::runtime_error("failed to open file!");

	size_t fileSize=input.tellg();
	std::vector<char> bytes(fileSize);

	input.seekg(0);
	input.read( bytes.data(), fileSize );
	input.close();

	initFromMemory( bytes.data(), bytes.size(), shaderStage );
}

vkraii::ShaderModule::ShaderModule( const vkraii::Device& device, void const* pData, size_t size, VkShaderStageFlagBits shaderStage )
	: device_( device.native_handle() )
{
	initFromMemory( pData, size, shaderStage );
}

vkraii::ShaderModule::ShaderModule( ShaderModule&& other )
	: device_( other.device_ ),
	  module_( other.module_ ),
	  shaderStageCreateInfo_( other.shaderStageCreateInfo_ )
{
	other.device_=VK_NULL_HANDLE;
	other.module_=VK_NULL_HANDLE;
}

vkraii::ShaderModule& vkraii::ShaderModule::operator=( ShaderModule&& other )
{
	if( this!=&other )
	{
		if( module_!=VK_NULL_HANDLE ) vkDestroyShaderModule( device_, module_, nullptr );
		device_=other.device_;
		module_=other.module_;
		shaderStageCreateInfo_=other.shaderStageCreateInfo_;
		other.device_=VK_NULL_HANDLE;
		other.module_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::ShaderModule::~ShaderModule()
{
	if( module_!=VK_NULL_HANDLE ) vkDestroyShaderModule( device_, module_, nullptr );
}

VkPipelineShaderStageCreateInfo& vkraii::ShaderModule::pipelineShaderStageCreateInfo()
{
	return shaderStageCreateInfo_;
}

const VkPipelineShaderStageCreateInfo& vkraii::ShaderModule::pipelineShaderStageCreateInfo() const
{
	return shaderStageCreateInfo_;
}

void vkraii::ShaderModule::initFromMemory( void const* pData, size_t size, VkShaderStageFlagBits shaderStage )
{
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType=VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize=size;
	createInfo.pCode=reinterpret_cast<const uint32_t*>(pData);

	VkResult result = vkCreateShaderModule( device_, &createInfo, nullptr, &module_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create shader module: ")+vkraii::tools::enumToString(result) );

	//
	// Now set up the CreateInfo structure
	// I haven't figured out how to customise this part yet.
	//
	shaderStageCreateInfo_.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageCreateInfo_.stage = shaderStage;
	shaderStageCreateInfo_.module = module_;
	shaderStageCreateInfo_.pName = "main";
}
