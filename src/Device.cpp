#include "vkraii/Device.h"
#include <string>
#include <stdexcept>
#include "vkraii/tools/enumToString.h"

vkraii::Device::Device( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Device::Device( VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo& createInfo )
{
	// Need to make a note of the queue family index in case it's needed later
	if( createInfo.pQueueCreateInfos==nullptr ) throw std::runtime_error("vkraii::Device - pQueueCreateInfos is null");
	queueFamilyIndex_=createInfo.pQueueCreateInfos->queueFamilyIndex;

	VkResult result = vkCreateDevice( physicalDevice, &createInfo, nullptr, &device_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create logical device: ")+vkraii::tools::enumToString(result) );
}

vkraii::Device::Device( Device&& other )
	: device_( other.device_ ),
	  queueFamilyIndex_( other.queueFamilyIndex_ )
{
	other.device_=VK_NULL_HANDLE;
}

vkraii::Device& vkraii::Device::operator=( Device&& other )
{
	if( this!=&other )
	{
		if( device_!=VK_NULL_HANDLE ) vkDestroyDevice( device_, nullptr );
		device_=other.device_;
		queueFamilyIndex_=other.queueFamilyIndex_;
		other.device_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Device::~Device()
{
	if( device_!=VK_NULL_HANDLE ) vkDestroyDevice( device_, nullptr );
}

VkQueue vkraii::Device::queueHandle( unsigned queueNumber )
{
	VkQueue queue;
	vkGetDeviceQueue( device_, queueFamilyIndex_, queueNumber, &queue );
	return queue;
}
