#include "vkraii/Semaphore.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::Semaphore::Semaphore( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Semaphore::Semaphore( const vkraii::Device& device )
	: device_(device.native_handle())
{
	VkSemaphoreCreateInfo createInfo={};
	createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	VkResult result = vkCreateSemaphore( device_, &createInfo, nullptr, &semaphore_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create semaphore: ")+vkraii::tools::enumToString(result) );
}

vkraii::Semaphore::Semaphore( Semaphore&& other )
	: device_( other.device_ ),
	  semaphore_( other.semaphore_ )
{
	other.device_=VK_NULL_HANDLE;
	other.semaphore_=VK_NULL_HANDLE;
}

vkraii::Semaphore& vkraii::Semaphore::operator=( Semaphore&& other )
{
	if( this!=&other )
	{
		if( semaphore_!=VK_NULL_HANDLE ) vkDestroySemaphore( device_, semaphore_, nullptr );
		device_=other.device_;
		semaphore_=other.semaphore_;
		other.device_=VK_NULL_HANDLE;
		other.semaphore_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Semaphore::~Semaphore()
{
	if( semaphore_!=VK_NULL_HANDLE ) vkDestroySemaphore( device_, semaphore_, nullptr );
}
