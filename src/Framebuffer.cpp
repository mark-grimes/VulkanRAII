#include "vkraii/Framebuffer.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::Framebuffer::Framebuffer( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Framebuffer::Framebuffer( const vkraii::Device& device, const VkFramebufferCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateFramebuffer( device_, &createInfo, nullptr, &framebuffer_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create framebuffer: ")+vkraii::tools::enumToString(result) );
}

vkraii::Framebuffer::Framebuffer( Framebuffer&& other )
	: device_( other.device_ ),
	  framebuffer_( other.framebuffer_ )
{
	other.device_=VK_NULL_HANDLE;
	other.framebuffer_=VK_NULL_HANDLE;
}

vkraii::Framebuffer& vkraii::Framebuffer::operator=( Framebuffer&& other )
{
	if( this!=&other )
	{
		if( framebuffer_!=VK_NULL_HANDLE ) vkDestroyFramebuffer( device_, framebuffer_, nullptr );
		device_=other.device_;
		framebuffer_=other.framebuffer_;
		other.device_=VK_NULL_HANDLE;
		other.framebuffer_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Framebuffer::~Framebuffer()
{
	if( framebuffer_!=VK_NULL_HANDLE ) vkDestroyFramebuffer( device_, framebuffer_, nullptr );
}
