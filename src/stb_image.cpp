/** @file Compilation unit for the stb_image.h definitions.
 *
 * The stb_image definitions could easily be included in another .cpp file (e.g. tools.cpp), but
 * having it in its own file allows turning off compiler warnings for just the stb_image.h code.
 * Xcode complains profusely about "possible misuse of comma", so it's useful to ignore warnings in
 * this particular file only.
 *
 * Note that this file is separate from the stb_image code, it is only to pull stb_image into this
 * project.
 */
#define STB_IMAGE_IMPLEMENTATION // Triggers the definitions, rather than just the declarations
#pragma GCC diagnostic push  // Clang also recognises these pragmas
#pragma GCC diagnostic ignored "-Wcomma"
#include <stb_image.h> // From <project root>/external/stb. Required for RawImage implementation.
#pragma GCC diagnostic pop
