#include "vkraii/PipelineCache.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::PipelineCache::PipelineCache( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::PipelineCache::PipelineCache( const vkraii::Device& device, const VkPipelineCacheCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreatePipelineCache( device_, &createInfo, nullptr, &pipelineCache_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create pipeline: ")+vkraii::tools::enumToString(result) );
}

vkraii::PipelineCache::PipelineCache( PipelineCache&& other )
	: device_( other.device_ ),
	  pipelineCache_( other.pipelineCache_ )
{
	other.device_=VK_NULL_HANDLE;
	other.pipelineCache_=VK_NULL_HANDLE;
}

vkraii::PipelineCache& vkraii::PipelineCache::operator=( PipelineCache&& other )
{
	if( this!=&other )
	{
		if( pipelineCache_!=VK_NULL_HANDLE ) vkDestroyPipelineCache( device_, pipelineCache_, nullptr );
		device_=other.device_;
		pipelineCache_=other.pipelineCache_;
		other.device_=VK_NULL_HANDLE;
		other.pipelineCache_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::PipelineCache::~PipelineCache()
{
	if( pipelineCache_!=VK_NULL_HANDLE ) vkDestroyPipelineCache( device_, pipelineCache_, nullptr );
}
