#include "vkraii/Instance.h"
#include <stdexcept>
#include <string>
#include <cstring>
#include "vkraii/tools/enumToString.h"

vkraii::Instance::Instance( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Instance::Instance( const VkInstanceCreateInfo& createInfo )
{
	VkResult result = vkCreateInstance( &createInfo, nullptr, &instance_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create instance: ")+vkraii::tools::enumToString(result) );

	// Take copies of all the strings of enabled layers so that I can implement the enabledLayers()
	// method without worrying about createInfo staying alive.
	for( size_t index=0; index<createInfo.enabledLayerCount; ++index )
	{
		size_t length=std::strlen( createInfo.ppEnabledLayerNames[index] )+1; // "+1" for null byte
		char* pNewString=static_cast<char*>( std::malloc(length) );
		std::strncpy( pNewString, createInfo.ppEnabledLayerNames[index], length );
		layers_.emplace_back( pNewString );
	}
}

vkraii::Instance::Instance( Instance&& other )
	: instance_( other.instance_ ),
	  layers_( std::move(other.layers_) )
{
	other.instance_=VK_NULL_HANDLE;
	other.layers_.clear(); // Pretty sure this is a byproduct of std::move above, but just to be sure
}

vkraii::Instance& vkraii::Instance::operator=( Instance&& other )
{
	if( this!=&other )
	{
		if( instance_!=VK_NULL_HANDLE ) vkDestroyInstance(instance_,nullptr);
		for( auto& pMemory : layers_ ) free( const_cast<char*>(pMemory) );
		instance_=other.instance_;
		layers_=std::move( other.layers_ );
		other.instance_=VK_NULL_HANDLE;
		other.layers_.clear(); // Pretty sure this is a byproduct of std::move above, but just to be sure
	}
	return *this;
}

vkraii::Instance::~Instance()
{
	if( instance_!=VK_NULL_HANDLE ) vkDestroyInstance(instance_,nullptr);

	// Free up the strings allocated in the constructor
	for( auto& pMemory : layers_ ) free( const_cast<char*>(pMemory) );
}

uint32_t vkraii::Instance::enabledLayerCount() const
{
	return static_cast<uint32_t>( layers_.size() );
}

const char* const* vkraii::Instance::ppEnabledLayerNames() const
{
	return layers_.data();
}

const std::vector<const char*>& vkraii::Instance::enabledLayers() const
{
	return layers_;
}
