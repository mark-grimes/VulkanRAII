#include "vkraii/DebugCallback.h"
#include <stdexcept>
#include <iostream>
#include "vkraii/Instance.h"

vkraii::DebugCallback::DebugCallback( vkraii::Instance& instance )
{
#ifndef NDEBUG
	if( instance.native_handle() != nullptr ) reInitiate( instance );
#endif
}

void vkraii::DebugCallback::reInitiate( vkraii::Instance& instance )
{
#ifndef NDEBUG
	if( destroyFunc_ && pCallback_ ) destroyFunc_( instance_, pCallback_, nullptr );

	initialisationFinished_=false;

	instance_=instance.native_handle();
	auto createFunc = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance_, "vkCreateDebugUtilsMessengerEXT");
	destroyFunc_ = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance_, "vkDestroyDebugUtilsMessengerEXT");
	if( createFunc==nullptr || destroyFunc_==nullptr )
	{
		std::cerr << "Couldn't get functions when creating vkraii::DebugCallback. No DebugUtilsMessengerEXT messages will be displayed.\n";
		destroyFunc_=nullptr; // Just to make sure the destructor doesn't try to call it
	}
	else
	{
		VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback_;
		createInfo.pUserData = this;

		VkResult result=createFunc( instance_, &createInfo, nullptr, &pCallback_ );
		if( result!=VK_SUCCESS )
		{
			destroyFunc_=nullptr;
			pCallback_=VK_NULL_HANDLE;
			std::cerr << "Couldn't set callback when creating vkraii::DebugCallback. No DebugUtilsMessengerEXT messages will be displayed.\n";
		}
	}

	initialisationFinished_=true;
#endif
}

vkraii::DebugCallback::~DebugCallback()
{
#ifndef NDEBUG
	if( destroyFunc_ && pCallback_ ) destroyFunc_( instance_, pCallback_, nullptr );
#endif
}

VKAPI_ATTR VkBool32 VKAPI_CALL vkraii::DebugCallback::debugCallback_(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData )
{
	vkraii::DebugCallback* pInstance=reinterpret_cast<vkraii::DebugCallback*>(pUserData);
	if( !pInstance->initialisationFinished_ ) return VK_FALSE; // Don't print "Added messenger" messages while setting up DebugCallback

	char const* severity;
	char const* type;
	switch( messageSeverity )
	{
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: severity="VERBOSE"; break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: severity="INFO"; break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: severity="WARNING"; break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: severity="ERROR"; break;
		default: severity="<unknown>";
	}
	switch( messageType )
	{
		case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT: type="GENERAL"; break;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: type="VALIDATION"; break;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: type="PERFORMANCE"; break;
		default: type="<unknown>";
	}
	std::cerr << "validation layer: " << severity << " " << type << " " << pCallbackData->pMessage << std::endl;
	return VK_FALSE;
}
