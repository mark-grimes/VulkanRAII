#include "vkraii/ImageView.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::ImageView::ImageView( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::ImageView::ImageView( const vkraii::Device& device, const VkImageViewCreateInfo& createInfo )
	: ImageView(device.native_handle(),createInfo)
{
	// No operation besides the initialiser list
}

vkraii::ImageView::ImageView( VkDevice device, const VkImageViewCreateInfo& createInfo )
	: device_(device)
{
	VkResult result = vkCreateImageView( device_, &createInfo, nullptr, &imageView_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create image view: ")+vkraii::tools::enumToString(result) );
}

vkraii::ImageView::ImageView( ImageView&& other )
	: device_(other.device_),
	  imageView_(other.imageView_)
{
	// Make sure the other instance doesn't call the destroy function when it destructs
	other.device_=VK_NULL_HANDLE;
	other.imageView_=VK_NULL_HANDLE;
}

vkraii::ImageView& vkraii::ImageView::operator=( ImageView&& other )
{
	if( this!=&other )
	{
		if( imageView_!=VK_NULL_HANDLE ) vkDestroyImageView(device_,imageView_,nullptr);
		device_=other.device_;
		imageView_=other.imageView_;
		// Make sure the other instance doesn't call the destroy function when it destructs
		other.device_=VK_NULL_HANDLE;
		other.imageView_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::ImageView::~ImageView()
{
	if( imageView_!=VK_NULL_HANDLE ) vkDestroyImageView(device_,imageView_,nullptr);
}
