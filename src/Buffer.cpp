#include "vkraii/Buffer.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/DeviceMemory.h"
#include "vkraii/tools/enumToString.h"

vkraii::Buffer::Buffer( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Buffer::Buffer( VkDevice device, const VkBufferCreateInfo& createInfo )
	: device_(device)
{
	VkResult result = vkCreateBuffer( device_, &createInfo, nullptr, &buffer_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't allocate memory: ")+vkraii::tools::enumToString(result) );
}

vkraii::Buffer::Buffer( const vkraii::Device& device, const VkBufferCreateInfo& createInfo )
	: Buffer( device.native_handle(), createInfo )
{
}

vkraii::Buffer::Buffer( Buffer&& other )
	: device_( other.device_ ),
	  buffer_( other.buffer_ )
{
	other.device_=VK_NULL_HANDLE;
	other.buffer_=VK_NULL_HANDLE;
}

vkraii::Buffer& vkraii::Buffer::operator=( Buffer&& other )
{
	if( this!=&other )
	{
		if( buffer_!=VK_NULL_HANDLE ) vkDestroyBuffer( device_, buffer_, nullptr );
		device_=other.device_;
		buffer_=other.buffer_;
		other.device_=VK_NULL_HANDLE;
		other.buffer_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Buffer::~Buffer()
{
	if( buffer_!=VK_NULL_HANDLE ) vkDestroyBuffer( device_, buffer_, nullptr );
}

VkMemoryRequirements vkraii::Buffer::memoryRequirements() const
{
	VkMemoryRequirements returnValue;
	vkGetBufferMemoryRequirements( device_, buffer_, &returnValue );
	return returnValue;
}

void vkraii::Buffer::bindBufferMemory( const vkraii::DeviceMemory& memory, VkDeviceSize offset )
{
	VkResult result = vkBindBufferMemory( device_, buffer_, memory.native_handle(), offset );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't bind buffer memory: ")+vkraii::tools::enumToString(result) );
}
