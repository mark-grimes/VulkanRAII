#include "vkraii/Surface.h"
#include <string>
#include "vkraii/Instance.h"
#include "vkraii/tools/enumToString.h"

vkraii::SwapChainSupportDetails::SwapChainSupportDetails( uint32_t formatsCount, uint32_t presentModesCount )
	: formats(formatsCount), presentModes(presentModesCount)
{
	// No operation besieds the initialiser list
}

vkraii::Surface::Surface( vkraii::Instance& instance, VkSurfaceKHR rawPointer )
	: instance_(instance.native_handle()),
	  surface_(rawPointer)
{
	// No operation besides the initialiser list
}

vkraii::Surface::Surface( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Surface::Surface( VkInstance instance, VkSurfaceKHR rawPointer )
	: instance_(instance),
	  surface_(rawPointer)
{
	// No operation besides the initialiser list
}

vkraii::Surface::Surface( Surface&& other )
	: instance_( other.instance_ ),
	  surface_( other.surface_ )
{
	other.instance_=VK_NULL_HANDLE;
	other.surface_=VK_NULL_HANDLE;
}

vkraii::Surface& vkraii::Surface::operator=( Surface&& other )
{
	if( this!=&other )
	{
		if( surface_!=VK_NULL_HANDLE ) vkDestroySurfaceKHR( instance_, surface_, nullptr );
		instance_=other.instance_;
		surface_=other.surface_;
		other.instance_=VK_NULL_HANDLE;
		other.surface_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Surface::~Surface()
{
	if( surface_!=VK_NULL_HANDLE ) vkDestroySurfaceKHR( instance_, surface_, nullptr );
}

bool vkraii::Surface::supportedByDevice( VkPhysicalDevice device, unsigned queueFamilyIndex ) const
{
	VkBool32 isSupported=false;
	vkGetPhysicalDeviceSurfaceSupportKHR( device, queueFamilyIndex, surface_, &isSupported );
	return isSupported;
}

vkraii::SwapChainSupportDetails vkraii::Surface::swapChainSupportDetails( VkPhysicalDevice device ) const
{
	uint32_t formatsCount;
	uint32_t presentModesCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface_, &formatsCount, nullptr );
	vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface_, &presentModesCount, nullptr );


	SwapChainSupportDetails details( formatsCount, presentModesCount );
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR( device, surface_, &details.capabilities );
	vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface_, &formatsCount, details.formats.data() );
	vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface_, &presentModesCount, details.presentModes.data() );
	return details;
}
