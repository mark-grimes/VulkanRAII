#include "vkraii/Image.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/DeviceMemory.h"
#include "vkraii/tools/enumToString.h"

vkraii::Image::Image( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Image::Image( const vkraii::Device& device, const VkImageCreateInfo& createInfo )
	: Image(device.native_handle(),createInfo)
{
	// No operation besides the initialiser list
}

vkraii::Image::Image( VkDevice device, const VkImageCreateInfo& createInfo )
	: device_(device),
	  format_(createInfo.format),
	  imageType_(createInfo.imageType)
{
	VkResult result = vkCreateImage( device_, &createInfo, nullptr, &image_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create image: ")+vkraii::tools::enumToString(result) );
}

vkraii::Image::Image( Image&& other )
	: device_(other.device_),
	  image_(other.image_),
	  format_(other.format_),
	  imageType_(other.imageType_)
{
	// Make sure the other instance doesn't call the destroy function when it destructs
	other.device_=VK_NULL_HANDLE;
	other.image_=VK_NULL_HANDLE;
}

vkraii::Image& vkraii::Image::operator=( Image&& other )
{
	if( this!=&other )
	{
		if( image_!=VK_NULL_HANDLE ) vkDestroyImage(device_,image_,nullptr);
		device_=other.device_;
		image_=other.image_;
		format_=other.format_;
		imageType_=other.imageType_;
		// Make sure the other instance doesn't call the destroy function when it destructs
		other.device_=VK_NULL_HANDLE;
		other.image_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Image::~Image()
{
	if( image_!=VK_NULL_HANDLE ) vkDestroyImage(device_,image_,nullptr);
}

VkFormat vkraii::Image::format() const
{
	return format_;
}

VkImageType vkraii::Image::imageType() const
{
	return imageType_;
}

VkMemoryRequirements vkraii::Image::memoryRequirements() const
{
	VkMemoryRequirements returnValue;
	vkGetImageMemoryRequirements( device_, image_, &returnValue );
	return returnValue;
}

void vkraii::Image::bindImageMemory( const vkraii::DeviceMemory& memory, VkDeviceSize offset )
{
	VkResult result = vkBindImageMemory( device_, image_, memory.native_handle(), offset );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't bind image memory: ")+vkraii::tools::enumToString(result) );
}
