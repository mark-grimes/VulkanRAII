#include "vkraii/Fence.h"
#include <stdexcept>
#include <limits>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/tools/enumToString.h"

vkraii::Fence::Fence( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Fence::Fence( const vkraii::Device& device, bool createSignaled )
	: device_(device.native_handle())
{
	VkFenceCreateInfo createInfo={};
	createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	if( createSignaled ) createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
	VkResult result = vkCreateFence( device_, &createInfo, nullptr, &fence_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create fence: ")+vkraii::tools::enumToString(result) );
}

vkraii::Fence::Fence( Fence&& other )
	: device_( other.device_ ),
	  fence_( other.fence_ )
{
	other.device_=VK_NULL_HANDLE;
	other.fence_=VK_NULL_HANDLE;
}

vkraii::Fence& vkraii::Fence::operator=( Fence&& other )
{
	if( this!=&other )
	{
		if( fence_!=VK_NULL_HANDLE ) vkDestroyFence( device_, fence_, nullptr );
		device_=other.device_;
		fence_=other.fence_;
		other.device_=VK_NULL_HANDLE;
		other.fence_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Fence::~Fence()
{
	if( fence_!=VK_NULL_HANDLE ) vkDestroyFence( device_, fence_, nullptr );
}

bool vkraii::Fence::status() const
{
	VkResult result=vkGetFenceStatus( device_, fence_ );
	if( result==VK_SUCCESS ) return true;
	else if( result==VK_NOT_READY ) return false;
	else throw std::runtime_error( std::string("Couldn't get fence status: ")+vkraii::tools::enumToString(result) );
}

void vkraii::Fence::wait() const
{
	vkWaitForFences( device_, 1, &fence_, VK_TRUE, std::numeric_limits<uint64_t>::max() );
}

void vkraii::Fence::reset()
{
	vkResetFences( device_, 1, &fence_ );
}
