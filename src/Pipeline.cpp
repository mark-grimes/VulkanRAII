#include "vkraii/Pipeline.h"
#include <stdexcept>
#include <string>
#include "vkraii/Device.h"
#include "vkraii/PipelineCache.h"
#include "vkraii/tools/enumToString.h"

vkraii::Pipeline::Pipeline( std::nullptr_t null )
{
	// No operation, the declaration has default values for the members
}

vkraii::Pipeline::Pipeline( const vkraii::Device& device, const VkGraphicsPipelineCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateGraphicsPipelines( device_, VK_NULL_HANDLE, 1, &createInfo, nullptr, &pipeline_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create pipeline: ")+vkraii::tools::enumToString(result) );
}

vkraii::Pipeline::Pipeline( const vkraii::Device& device, const VkComputePipelineCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateComputePipelines( device_, VK_NULL_HANDLE, 1, &createInfo, nullptr, &pipeline_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create pipeline: ")+vkraii::tools::enumToString(result) );
}

vkraii::Pipeline::Pipeline( const vkraii::Device& device, vkraii::PipelineCache& pipelineCache, const VkGraphicsPipelineCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateGraphicsPipelines( device_, pipelineCache.native_handle(), 1, &createInfo, nullptr, &pipeline_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create pipeline: ")+vkraii::tools::enumToString(result) );
}

vkraii::Pipeline::Pipeline( const vkraii::Device& device, vkraii::PipelineCache& pipelineCache, const VkComputePipelineCreateInfo& createInfo )
	: device_(device.native_handle())
{
	VkResult result = vkCreateComputePipelines( device_, pipelineCache.native_handle(), 1, &createInfo, nullptr, &pipeline_ );
	if( result!=VK_SUCCESS ) throw std::runtime_error( std::string("Couldn't create pipeline: ")+vkraii::tools::enumToString(result) );
}

vkraii::Pipeline::Pipeline( Pipeline&& other )
	: device_( other.device_ ),
	  pipeline_( other.pipeline_ )
{
	other.device_=VK_NULL_HANDLE;
	other.pipeline_=VK_NULL_HANDLE;
}

vkraii::Pipeline& vkraii::Pipeline::operator=( Pipeline&& other )
{
	if( this!=&other )
	{
		if( pipeline_!=VK_NULL_HANDLE ) vkDestroyPipeline( device_, pipeline_, nullptr );
		device_=other.device_;
		pipeline_=other.pipeline_;
		other.device_=VK_NULL_HANDLE;
		other.pipeline_=VK_NULL_HANDLE;
	}
	return *this;
}

vkraii::Pipeline::~Pipeline()
{
	if( pipeline_!=VK_NULL_HANDLE ) vkDestroyPipeline( device_, pipeline_, nullptr );
}
